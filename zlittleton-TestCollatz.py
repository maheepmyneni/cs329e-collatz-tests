#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------

class TestCollatz (TestCase):
    
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100)
        self.assertEqual(j, 200)

    def test_read_3(self):
        s = "201 210\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 201)
        self.assertEqual(j, 210)

    def test_read_4(self):
        s = "900 1000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 900)
        self.assertEqual(j, 1000)


    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(10, 20)
        self.assertEqual(v, 21)

    def test_eval_2(self):
        v = collatz_eval(5, 15)
        self.assertEqual(v, 20)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self): #case that i=j
        v = collatz_eval(5, 5)
        self.assertEqual(v, 6)

    def test_eval_6(self): #larger (likely uncached) i=j
        v = collatz_eval(2483, 2483)
        self.assertEqual(v, 41)

    def test_eval_7(self): #case that i>j
        v = collatz_eval(50, 28)
        self.assertEqual(v, 110)

    def test_eval_8(self): #i>j and overlapping intervals
        v = collatz_eval(841,1309)
        self.assertEqual(v, 182)

    def test_eval_9(self): #some larger numbers
        v = collatz_eval(20000, 35302)
        self.assertEqual(v, 311)

    def test_eval_10(self): #near end of range
        v = collatz_eval(999990,999999)
        self.assertEqual(v, 259)

    def test_eval_11(self): #large interval
        v = collatz_eval(30000,300000)
        self.assertEqual(v, 443)

    def test_eval_12(self): #was having trouble with this case earlier
        v = collatz_eval(2,3)
        self.assertEqual(v,8)

    def test_eval_13(self): #was having trouble with this case earlier
        v = collatz_eval(1,4)
        self.assertEqual(v,8)

    def test_eval_14(self): #and this one
        v = collatz_eval(1,2)
        self.assertEqual(v,2)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self): #case i>j: print as given (in flipped form)
        w = StringIO()
        collatz_print(w, 10, 1, 20)
        self.assertEqual(w.getvalue(), "10 1 20\n")

    def test_print_3(self): #case i=j
        w = StringIO()
        collatz_print(w, 5, 5, 6)
        self.assertEqual(w.getvalue(), "5 5 6\n")

    def test_print_4(self): #large numbers
        w = StringIO()
        collatz_print(w, 20000, 35302, 311)
        self.assertEqual(w.getvalue(), "20000 35302 311\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("10 1\n5 5\n20000 35302\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 1 20\n5 5 6\n20000 35302 311\n")

    def test_solve_3(self): #for coverage
        r = StringIO("1583 1583\n800 2000\n7 7\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1583 1583 167\n800 2000 182\n7 7 17\n")

    
    def test_solve_4(self): #max case
        r = StringIO("1 999999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 999999 525\n")

    def test_solve_5(self): #in case there's an empty line
        r = StringIO("\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual (
            w.getvalue(), "  \n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage-3.5 run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage-3.5 report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
